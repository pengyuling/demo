const NAME = 'mgm'
function getRes(res) {
  return {...res, ...{name: NAME }}
}
const state = {
  m1Result: { //m1的邀请结果
    inviteRedAmt: {
      inviteSum: 0, //成功邀请人数
      redAmt: 0, //红包总金额
      awardCount: 0, //优惠券数量
    },
    sucM1Number: 0, //活动总邀请成功的m1数量
    ranking: []
  },
  mgmCode: '', //mgm子活动编码
  inviteCode: '', //邀请码
  inviteUrl: '', //邀请链接
  m1Info: {}, //通过邀请码获取的m1的信息
}
const mutations = {
}
const actions = {
  /**
   * 设置邀请码和分享
   * @param {string} payload 邀请码
   */
  setInviteCode({state, rootState}, payload) {
    if(!payload) return
    state.inviteCode = payload
    let _share = { ...rootState.mpsComponents['mps-share'].data }
    _share.activityCode = rootState.mpsComponents.common.data.actCode
    state.inviteUrl = BaseTool.replaceParam(_share.link.trim()? _share.link.trim(): location.href, {
      mgm: state.inviteCode
    })
    _share.link = state.inviteUrl
    Share.init(_share)
  },
  /**
   * 创建邀请码 
   * @param {Object} payload 
   *    cb 创建邀请码成功回调
   *    name 组件名
   */
  async createInviteCode({state, commit, dispatch}, payload = {}) {
    if (state.inviteCode) { //已经创建过邀请码
      payload.cb && payload.cb()
      return
    }
    let _opts = { activityPreCode: state.mgmCode }
    const res = await Api('mgm/create', _opts)
    if (res.type == 'success') {
      if (res.data.inviteCode) {
        dispatch('setInviteCode', res.data.inviteCode)
        payload.cb && payload.cb()
      } else {
        commit('setTips', getRes({type: 'error', errcode: 'E_SYSTEM_ERROR'}), {root: true})
      }
    } else {
      commit('setTips', getRes(res), {root: true})
    }
  },
  /**
   * 初始化m1的邀请结果
   */
  async initM1({state}) {
    let _opts = {
      activityPreCode: state.mgmCode,
      queryRank: false
    }
    const res = await Api('mgm/initM1', _opts)
    if (res.type == 'success') {
      if (res.data) {
        state.m1Result = res.data || {}
      }
    }
  },
  /**
   * 通过邀请码获取m1信息，通常用于在m2页面展示m1头像、昵称
   */
  async getM1InfoByInviteCode({state}) {
    const res = await Api('queryM1ByInviteCode', {
      activityPreCode: state.mgmCode,
      inviteCode: state.inviteCode
    })
    if (res.type == 'success') {
      state.m1Info = res.data || {}
    }
  },
  /**
   * 保存邀请关系
   * @param {Object} payload 
   *  params 绑定邀请关系是的接口入参，主要用于通过手机号验证码的方式绑定时传
   */
  async saveRelation({state, commit}, payload = {}) {
    let _opts = {
      activityPreCode: state.mgmCode,
      inviteCode: state.inviteCode,
      type: 1
    }
    if(payload.params) Object.assign(_opts, payload.params)
    const res = await Api('mgm/save', _opts)
    if (res.type == 'success') {
      commit('setTips', getRes({ key: 'INVITE_SUCCESS' }), {root: true})
    } else {
      commit('setTips', getRes(res), {root: true})
    }
  },
  /**
   * 创建邀请码并调起分享组件
   */
  async linkShare({ rootState, commit, dispatch }) {
    if (BaseTool.checkUA('App') || BaseTool.checkUA('Wechat') || BaseTool.checkUA('AliPay')) {
      if (rootState.mpsComponents.common.data.needMgm) {
        dispatch('createInviteCode', {
          cb: () => { Share.call() }
        })
      } else {
        Share.call()
      }
    } else {
      commit('setTips', getRes({ type:'error', errcode: 'E_ACT_CHANNEL_ERROR' }), {root: true})
    }
  },
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}