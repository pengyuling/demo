import Vue from 'vue';
import Vuex from 'vuex';
import constantTipsList from '../assets/config/tips-list';
import mgm from './mgm'

Vue.use(Vuex);

// initial state
const state = {
  coverShow: false, // 蒙层显示隐藏
  userInfo: null, // 用户信息
  mpsComponents: null, // 组件数据
  actCode: null,// 活动编码
  drawInfo: null,
  popShow: false, // 控制所有弹窗显示跟隐藏
  noticeActCode: null, // 中奖公告编码
  popConfig: {
    title: '温馨提示',
    content: '弹窗内容',
    btnName: '确定',
    btnFn: () => {
      console.log('关闭');
    }
  },// 错误弹窗配置
}

// getters
const getters = {
}

// actions
const actions = {
  async getUserInfo({ state }) {
    const res = await Api('common/checkUserInfo', {
    });
    if (res.type == 'success') {
      state.userInfo = res.data;
      if(state.userInfo) {
        if (BaseTool.checkUA('AliPay')) {
          if (!state.userInfo.authInfo || !state.userInfo.authInfo.isAuth) {
            Auth.aliAuth()
          }
        }
      }
    }
  },
  async checkStatus({ state, commit }) {
    const res = await Api('common/checkStatus', {
      activityPreCode: state.mpsComponents['mps-draw'].data.actCode // 输入对应的子活动编码
    });
    if (res.type == 'error') {
      // 活动状态异常
      const endLink = state.mpsComponents.common.data.endLink
      if (res.errcode === 'E_INIT_NO_ACTIVITY' && endLink) {
        window.HandleLink(endLink)
        return true
      }
      commit('setTips', res);
      return true;
    } else {
      return false;
    }
  }
}

// mutations
const mutations = {
  // 控制弹窗显示隐藏
  setDialogShow(state, payload) {
    // 设置弹窗显示隐藏
    state.popShow = payload.data;
    state.coverShow = typeof payload.data == 'boolean' ? false : true;
  },
  setTips(state, payload) {
    // 设置弹窗
    let config = state.mpsComponents['mps-tips'].data.list;
    let obj = HandleTips.extend(config, constantTipsList, payload)
    if (obj !== false) {
      // 处理直接函数的情况
      state.popConfig = BaseTool.deepExtend(state.popConfig, obj)
      state.popShow = 'tips';
      state.coverShow = true;
    }
    if (payload) state.popConfig.data = BaseTool.clone(payload)
  },
  // 抽奖相关
  setDrawInfo(state, drawInfo) {
    // 设置抽奖信息
    state.drawInfo = drawInfo
  },
  // 规则相关
  showRuleDialog(state) {
    state.popShow = 'rule';
    state.coverShow = true;
  },
  setComponents(state, components) {
    state.mpsComponents = components;
    state.actCode = components['common'].data.actCode;
    state.noticeActCode = components['mps-draw'].data.actCode;
    if (components.common.data.needMgm) {
      state.mgm.mgmCode = components.common.data.mgmCode
    }
  }
}

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions,
  modules: {
    mgm,
  }
});
