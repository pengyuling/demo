import Vue from 'vue';
import Index from './pages/index.vue';
import { 
  mpsBanner,
  mpsDraw,
  mpsAdvert,
  mpsRulePop,
  mpsCover,
  mpsDialog,
  mpsTips,
  mpsTitle,
  mpsFooter,
} from '@mps/components';
import './assets/style/global.css';
import './assets/style/style.css';
import store from './store';
Vue.use(mpsBanner);
Vue.use(mpsDraw);
Vue.use(mpsAdvert);
Vue.use(mpsRulePop);
Vue.use(mpsCover);
Vue.use(mpsDialog);
Vue.use(mpsTitle);
Vue.use(mpsTips);
Vue.use(mpsFooter)
new Vue({
  el: '#app',
  store,
  render: h => h(Index)
})