export default {
  'mps-draw': {
    offeringFlag: false,
    type: 1,
    preOperation() {
    },
    nextOperation(result) {
    },
  },
  'mps-tips': {
    iconType: 2
  },
  'mps-advert': {
    fullscreen: false,
    spaceX: true,
    spaceY: true //spaceY支持1-9的数字来控制间距
  }
}
