export default {
  "common": {
    "title": "公共配置区",
    "data": {
      "actCode": "wechat_june_actCode",
      "pageCode": "01"
    }
  },
  "mps-banner": {
    "title": "头图区",
    "data": {
      "src": require('../assets/images/header.png'),
      "hasRuleBtn": true
    }
  },
  "mps-draw": {
    "title": "抽奖区",
    "data": { 
      "actCode": "",
      "couponList": [
      ],
      "emptyIndex": "",
      "remainSrc": "",
      "awardSrc": require('../assets/images/common_ninebox_prize.png'),
      "bgSrc": require('../assets/images/common_ninebox.png')
    }
  },
  "mps-advert": {
    "title": "广告区",
    "data": {
      "list": [
        [
        ] 
      ]
    }
  },
  "mps-share": {
    "title": "分享区",
    "data": {
      "title": "招联金融",
      "desc": "招联金融",
      "imgUrl": "https://act.mucfc.com/act/staticImgs/share.png",
      "link": ""
    }
  }
}