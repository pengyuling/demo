function deepObjectMerge(firstObj, secondObj) {
  for (var key in secondObj) {
    if (secondObj[key] === "") continue;
    let _firstObjType = Object.prototype.toString.call(firstObj[key]), _secondObjType = Object.prototype.toString.call(secondObj[key])
    if (_firstObjType === _secondObjType) {
      if (_secondObjType === "[object Object]") {
        deepObjectMerge(firstObj[key], secondObj[key]);
        continue;
      } else if (_secondObjType === "[object Array]" && secondObj[key].length === 0) {
        continue;
      }
    }

    firstObj[key] = secondObj[key];
  }
  return firstObj;
}

export {
  deepObjectMerge
}