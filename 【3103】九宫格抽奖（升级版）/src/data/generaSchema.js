const fs = require('fs');
const path = require('path');
const file = fs.readdirSync(__dirname);
// 获取参数
const params = process.argv.slice(2);
const fileArray = file.filter((file) => {
  if (params.length > 0) {
    if (new RegExp(params.join("|"),'gi').test(file) && !/schema|base\-config/gi.test(file) && /.*json/gi.test(file)) {
      return true;
    }
  } else {
    if (!/schema|base\-config/gi.test(file) && /.*json/gi.test(file)) {
      return true;
    }
  }
});

const promiseArray = [];
fileArray.forEach((file) => {
  promiseArray.push(new Promise((resolve, reject) => {
    process.nextTick(() => {
      generatorSchema(file, resolve, reject);
    });
  }))
});

Promise.all(promiseArray).then(() => {
  console.log('done!');
});

function generatorSchema(file, resolve, reject) {
  let data = fs.readFileSync(path.resolve(__dirname, file), 'utf-8');
  const result = {};
  data = JSON.parse(data);
  Object.keys(data).forEach((key) => {
    const configData = data[key].data;
    generatorProperties(result, key, configData, data[key].title);
  });
  fs.writeFileSync(path.resolve(__dirname, file.split('.')[0] + '-schema.json'), JSON.stringify(result), 'utf-8');
}

function generatorProperties(obj, key, value, title) {
  obj[key] = {};
  obj[key].title = title.replace('区', '配置');
  obj[key].type = dataType(value).toLowerCase();
  let format = geneFormat(key);
  if (format) obj[key].format = format;
  if ('object' === dataType(value).toLowerCase()) {
    obj[key].properties = {};
    Object.keys(value).forEach((propertiesKey) => {
      // value[propertiesKey] 根据字段属性值生成名称
      // propertiesKey 根据字段名生成名称，字段名应该遵从某个规范
      generatorProperties(obj[key].properties, propertiesKey, value[propertiesKey], geneTitle(propertiesKey));
    });
  } else if ('array' === dataType(value).toLowerCase()) {
    obj[key].items = {
      title: "列表项",
      type: "object",
      properties: {}
    };
    if (!value[0]) return;
    value = value[0];
    Object.keys(value).forEach((propertiesKey) => {
      // value[propertiesKey] 根据字段属性值生成名称
      // propertiesKey 根据字段名生成名称，字段名应该遵从某个规范
      generatorProperties(obj[key].items.properties, propertiesKey, value[propertiesKey], geneTitle(propertiesKey));
    });
  }
}

function dataType(data) {
  return Object.prototype.toString.call(data).slice(8, -1)
}

// 生成标题(可以根据情况调整如下映射)
function geneTitle(key) {
  let keyArray = [
    {name: "图片", match: 'src|image|img|pic|icon'}, // 含有src, image, img, pic, icon 则转化为“图片”
    {name: "按钮", match: 'btn|button'}, // 含有btn, button 则转化为“按钮”
    {name: '子',   match: 'sub'}, // 含有sub 则转化为“子”
    {name: '活动', match: 'act'}, // 含有act 则转化为“活动”
    {name: "编码", match: 'code'}, // 含有code 则转化为“编码”
    {name: "链接", match: 'link|url'}, // 含有link,url 则转化为“链接”
    {name: "列表", match: 'list'}, // 含有list  则转化为“列表”
    {name: "描述", match: 'desc'}, // 含有desc  则转化为“描述”
    {name: "标题", match: 'title'}, // 含有title 则转化为“标题”
    {name: "名称", match: 'name'}, // 含有name 则转化为“名称”
    {name: "序号", match: 'index'} // 含有index 则转化为“序号”
  ];
  let o = {};
  keyArray.map(item => {
    let match = item.match.split("|");
    match.map(mi => {
      o[mi] = item.name;
    })
  });
  let keyNames = o;
  for (let p in keyNames) {
    key = key.toLowerCase().replace(new RegExp(p, 'i'), function (result) {
      return keyNames[result];
    });
  }
  return key;
}

// 生成format约束(可以根据情况调整如下映射)
function geneFormat(key) {
  // 含有src, image, img, pic, icon 则转化为“format:custom-image”
  // 含有actCode 则转化为“format:custom-activity-code”
  // 含有subActCode 则转化为“format:custom-children-code”
  // 含有rules text 则转化为"format:custom-rich-text"
  const keyNames = {
    "src": "custom-image",
    "img": "custom-image",
    "image": "custom-image",
    "pic": "custom-image",
    "icon": "custom-image",
    "subActCode": "custom-children-code",
    "actCode": "custom-activity-code",
    "rules": "custom-rich-text",
    "text": "custom-rich-text"
  }
  let keymap = "";
  let keyMaps = Object.keys(keyNames);
  for (let p = 0; p < keyMaps.length; p++) {
    if (key.toLowerCase().indexOf(keyMaps[p].toLowerCase()) > -1) {
      keymap = keyNames[keyMaps[p]];
      break;
    }
  }
  return keymap;
}
