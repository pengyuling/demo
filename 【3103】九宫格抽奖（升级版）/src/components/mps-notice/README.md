# 公告组件

-----

快速生成公告组件

-----

## 使用说明

```
  <mps-notice :config="noticeConfig" :options="noticeOptions"></mps-notice>
  <script>
    import mpsNotice from 'common/mps-notice';
    export default {
      components: {
        mpsNotice
      },
      data() {
        return {
          noticeConfig: {
            // 静态配置数据(标题等)
          },
          noticeOptions: {
            // 公告组件配置数据
          }
        }
      }
    }
  </script>
```

## 参数列表

| 参数       |          参数意义          |   参数类型   | 是否必须 |
| ---------- | :------------------------: | :----------: | :------: |
| config     |      公告静态配置参数      |    Object    |    是    |
| -title     | 标题配置(具体见mps-title） |    Object    |    否    |
| options    |        公告配置参数        |    Object    |    是    |
| -queryNum  |   公告查询条数，默认10条   |    Number    |    否    |
| -animation |        是否自动滚动        |   Boolean    |    否    |
| slot       |            插槽            | 公告内容列表 |    是    |

## 参数示例

```
  config: {
    title: Object(具体见mps-static)
  },
  options: {
    queryNum: 30,
    animation: true
  }
```

## VUEX交互

### State

#### noticeList、noticeActCode

公告列表与活动编码
```
  const state = {
    noticeList: {

    },
    noticeActCode: 'xxx'
  }
```

### Mutation

#### setNoticeList
设置公告列表
```
  const state = {
    noticeList: {

    },
    noticeActCode: 'xxx'
  }
  const mutations = {
    setNoticeList(state, noticeList) {
      // 设置中奖公告列表
      state.noticeList = noticeList;
    }
  }
```
