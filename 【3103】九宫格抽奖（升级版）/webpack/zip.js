const fs = require('fs');
const path = require('path');
const archiver = require('archiver');
// 获取构建环境参数
const params = process.argv.slice(2);

let date = new Date();
// 获取项目名称
let pjName = process.cwd().substring(process.cwd().lastIndexOf('\\') + 1);

const libraryName = `${params.join()}_${pjName}_${date.getFullYear()}_${date.getMonth() + 1}_${date.getDate()}.zip`;

//const libraryName = 'farm-template.zip';

const outputPath = path.join(__dirname, '..', libraryName);
const output = fs.createWriteStream(outputPath);

const archive = archiver('zip', {
  zlib: {
    level: 9
  }
});

output.on('close', () => {
  console.log(`  ${archive.pointer()} total bytes`);
});
output.on('end', () => {
  console.log('Data has been drained');
});
archive.on('warning', (err) => {
  if (err.code === 'ENOENT') {
    // log warning
  } else {
    // throw error
    throw err;
  }
});
archive.on('error', (err) => {
  throw err;
});

archive.pipe(output);

const directoryList = [
  'server'
];
const fileList = [

];

directoryList.forEach((dirPath) => {
  archive.directory(path.join(__dirname, '..', dirPath), dirPath);
});
fileList.forEach((filePath) => {
  archive.file(path.join(__dirname, '..', filePath), { name: filePath });
});

archive.finalize();
