const argv = require('yargs').argv;
const buildMode = argv.m || 'st1';
console.log(buildMode)
const actHost = {
  st: 'http://act-st.cfcmu.cn',
  st1: 'http://st1.act.cfcmu.cn',
  st2: 'http://st2.act.cfcmu.cn',
  prod: 'https://act.mucfc.com',
  uat: 'http://act-uat.cfcmu.cn'
};
const zlHost = {
  st: 'http://m-zl-st.cfcmu.cn',
  st1: 'http://m-zl-st1.cfcmu.cn',
  st2: 'http://m-zl-st2.cfcmu.cn',
  prod: 'https://m-zl.mucfc.com',
  uat: 'http://m-zl-uat.cfcmu.cn'
}
module.exports = {
  // 招联金融接入链接前缀部分
  buildMode,
  // 活动系统接入链接前缀部分
  actHost: actHost[buildMode],
  zlHost: zlHost[buildMode]
};