const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin'); //清理dist重复文件
const webpack = require('webpack');
const config = require('./config');
const htmlWepackPlugin = require('html-webpack-plugin'); //使用html模板
const buildDir = 'server';
const CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports = function (mode) {
  return {
    entry: {
      index: [path.resolve(__dirname, '../src/index.js')], //定义入口文件，./表示根目录的相对路径 babel-polyfill for state-2
      vendor: ["vue", "vue-router"]
    },
    output: {
      path: path.resolve(__dirname, '../' + buildDir),
    },
    module: {
      rules: [{
        test: /\.jsx?$/,
        //exclude: path.resolve(__dirname, '../../node_modules/'),
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.vue$/,
        //exclude: path.resolve(__dirname, '../../node_modules/'),
        use: [
          'vue-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            outputPath: 'images/'
          }
        }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            outputPath: 'fonts/'
          }
        }]
      }
      ]
    },
    resolve: {
      alias: {
        'common': path.resolve(__dirname, '../src/components'),
        'lib': path.resolve(__dirname, "../src/lib"),
        'vue$': 'vue/dist/vue.esm.js'
      },
      extensions: ['.js', '.vue', '.json', '.css', '.less', '.jsx']
    },
    plugins: [
      new CleanWebpackPlugin(
        ['./' + buildDir + '/*'], 　 //匹配删除的文件
        {
          root: path.resolve(__dirname, '../'),//根目录
          verbose: true,//开启在控制台输出信息
          dry: false//启用删除文件
        }
      ),
      new htmlWepackPlugin({
        title: '招联金融',
        template: path.resolve(__dirname, '../template/index.html'),
        filename: 'index.html',
        actDomain: config['actHost'],
        zlDomain: config['zlHost'],
        chunks: ['vendor', 'mainfest', 'index']
      }),

      new webpack.DefinePlugin({
        'process.env.NODE_ENV': config.buildMode == 'prod' ? JSON.stringify('production') : JSON.stringify('development')
      }),

      new webpack.optimize.CommonsChunkPlugin({
        names: ['vendor', 'mainfest'] // 指定公共 bundle 的名称,mainfest为自动生成运行时代码，详见https://doc.webpack-china.org/guides/caching
      }),

      new CopyWebpackPlugin([
        { from: 'src/data/index.json', to: './config' },
        { from: 'src/data/index-schema.json', to: './config' },
        { from: 'src/data/base-config.json', to: './config' },
        { from: 'src/data/base-config-schema.json', to: './config' }
      ])
    ]
  }
}
