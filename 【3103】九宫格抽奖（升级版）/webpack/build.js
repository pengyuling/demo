const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const webpackConfig = require('./webpack.prod.conf');
const config = require('./config');
const imgUrl = config.actHost;

const pageList = getPageList();
const dataPath = path.resolve(__dirname, '../src/data');

pageList.forEach((page) => {
  const file = path.resolve(dataPath, `./${page}.json`);
  let data = fs.readFileSync(file, 'utf-8');
  data = data.replace(/(http:\/\/)?st1\.act\.cfcmu\.cn/gi, imgUrl);
  fs.writeFileSync(file, data, 'utf-8');
});

webpack(webpackConfig, (err, stats) => {
  if (err) throw err;
  process.stdout.write(stats.toString({
    colors: true,
    modules: false,
    children: false, // If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
    chunks: false,
    chunkModules: false
  }) + '\n\n')

  if (stats.hasErrors()) {
    console.log('  Build failed with errors.\n')
    process.exit(1)
  }

  console.log('  Build complete.')
})


// 获取页面列表
function getPageList() {
  const templatePiplineDir = path.resolve(__dirname, '../template');
  const list = fs.readdirSync(templatePiplineDir);
  const pagesArray = [];
  list.forEach((file) => {
    if (/origin/gi.test(file) || !(/\.html/gi.test(file))) return;
    pagesArray.push(file.split('.html')[0]);
  });
  return pagesArray;
}