const webpackDevServer = require("webpack-dev-server");
const webpack = require('webpack');
const webpackConfig = require("./webpack.dev.conf");
const port = 8080;
for (let i in webpackConfig.entry) {
  if (i !== 'vendor') {
    webpackConfig.entry[i].unshift(`webpack-dev-server/client?http://0.0.0.0:${port}/`, 'webpack/hot/only-dev-server');
  }
}

var compiler = webpack(webpackConfig);
var server = new webpackDevServer(compiler, {
  disableHostCheck: true,
  hot: true,
  stats: {
    colors: true,
    modules: false,
    children: false,
    chunks: false,
    chunkModules: false
  }
});

server.listen(port, '0.0.0.0', function (err) {
});


