const webpack = require('webpack');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.comm.conf')('dev');
const postCssConf = {
  loader: 'postcss-loader',
  options: {
    ident: 'postcss',
    plugins: (loader) => [
      require('autoprefixer')()
    ]
  }
}
module.exports = merge(baseWebpackConfig, {
  module: {
    rules: [{
      test: /\.css$/,
      use: ["style-loader", "css-loader", postCssConf]
    },
    {
      test: /\.less$/,
      use: ["style-loader", "css-loader", postCssConf, 'less-loader']
    }
    ]
  },
  output: {
    filename: '[name].js',
    publicPath: '/' //定义资源引用路径
  },

  devtool: 'cheap-module-eval-source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
});