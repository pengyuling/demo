const webpack = require('webpack');
const merge = require('webpack-merge');

const ExtractTextPlugin = require("extract-text-webpack-plugin"); //分离css
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const baseWebpackConfig = require('./webpack.comm.conf')('prod');
const postCssConf = {
  loader: 'postcss-loader',
  options: {
    ident: 'postcss',
    plugins: (loader) => [
      require('autoprefixer')()
    ]
  }
}
module.exports = merge(baseWebpackConfig, {
  module: {
    rules: [{
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: [{
          loader: "css-loader",
          options: {
            minimize: true
          }
        },
          postCssConf
        ]
      })
    },
    {
      test: /\.less$/,
      use: ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: [{
          loader: "css-loader",
          options: {
            minimize: true
          }
        },
          postCssConf,
        {
          loader: 'less-loader'
        }
        ]
      })
    }
    ]
  },
  output: {
    filename: '[name].[chunkhash:8].js',
    publicPath: './' //针对里面的静态资源定位，比如html引用的js位置
  },
  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(), //一定要用es6的模块引入，es5会回退，还是会导致引入新文件使vendor的版本号变化
    //new webpack.HashedModuleIdsPlugin(),//防止vendors跟随本地依赖变更而变更版本号，目前有ModuleConcatenationPlugin就暂不使用
    new UglifyJSPlugin(),
    new ExtractTextPlugin({
      filename: '[name].[contenthash:8].css',
      allChunks: true
    })
  ]
});